# PortableISP

**NOTE:** THIS PROJECT IS NO LONGER MAINTAINS AND HAS BEEN SUPERSEDED BY THE PORTAPROG


An AVR ISP Programmer and UART Terminal interface built with an ESP32.

IMPORTANT: This project was developed using the **LilyGO TTGO ESP32 w/ 18650 Charging & 0.96" OLED** board and
takes advantage of many of its features including the on-board OLED and the BOOT button.

Supports direct firmware flashing over TCP and reflashing using firmware stored on the SPIFFS.

Supports telnet for remote UART/Serial terminal

**TODO:**
  - include a simple web browser interface as a UART/Serial terminal
  - add SSL support for browser connections to the web server and websocket interface

![Portable ISP Programmer](https://gitlab.com/bradanlane/portableisp/raw/master/files/portableisp_small.jpg)

--------------------------------------------------------------------------

--------------------------------------------------------------------------
### ISP API for AVR chips
Provides the interface for flashing AVR chips - ATtiny85, ATmega328P, etc - using ISP

There are two methods of using the ISP Programmer:
  - a network addressable programmer via TCP usable with software such as `avrdude`
  - a self contained programmer using files located in the SPIFFS file system

Example TCP usage: `avrdude -b 230400 -c arduino -p t85 -P net:<address>:<port> -U flash:w:default.hex`

_The current `IP address` and `port` are displayed on the OLED during the startup process._

The SPIFFS flash interface is handled by the combination of the OLED display and the UI button.
The display starts with the list of available files.
A short press of the button will step through the list of available files.
A long press will initiation the flash process for the currently highlighted file.
The flash process will display progress messages on the OLED.
A final press of the button returns to the file list.

#### ispInit()
Performs all necessary initialization of ISP programming systems.
Must be called once before using any of the other ISP operations.
 - return: **bool** `true` on success and `false` on failure

#### ispLoop()
Give the ISP processes an opportunity to respond to events

#### ispFlashFile()
mark an SPIFFS file to be flash

Note: This function does not perform the flash operation. 
It only saves the file name. 
The flash operation will be handled during the next loop. 
Also, this is not a queuing operation. 
If a file name is already waiting to be flashed, this will usurp.

--------------------------------------------------------------------------
### AVR ISP TCP Interface

provide AVR programming using the STK500 protocol over a TCP connection

**WARNING**: The license agreements for the entire pedigree of the AVR ISP TCP code base 
spans GLPL2, BSD, and MIT licenses. It's not clear anyone has been able to decipher the 
actual rights granted to the end user. All of this is likely because _an open source
"license" should not be over 4000 words long and filling nearly 10 pages_. 
Obviously this is not an acceptable excuse but it's better than _my dog ate my homework_.

**NOTE**: There are a number of ESP8266 based implementations for an ISP programmer.
The switch to the ESP32 requires the explicit declaration of the
the pin assignments for the hardware SPI and to set the bit/byte order.

	`SPI.begin(PIN_ISP_CLK, PIN_ISP_MISO, PIN_ISP_MOSI);`
	`SPI.setBitOrder(MSBFIRST);`


--------------------------------------------------------------------------
### AVR ISP SPIFFS Interface

Provides AVR flashing using standard HEX files stored on SPIFFS file system

--------------------------------------------------------------------------
### OLED API
Provides the display and button UI using and SSD1306 I2C OLED display module

The display and UI interface supports text-line based display functions.

The UI consists of a single button which reports its current state.

There are three display modes:
  - file list - a list of the files currently in SPIFFS storage
  - file menu - list of options available fore the currently selected file
  - text msg - a scrolling screen of messages for the current operation


#### oledInit()
Performs all necessary initialization of both the OLED and the button(s)
Must be called once before using any of the other display functions.
 - return: **bool** `true` on success and `false` on failure

#### oledLoop()
Give the OLED and button(s) an opportunity to respond to interaction events and update the display

#### oledClear()
delete all current content and mark display buffer as dirty so it is refreshed during the next cycle

- input: **bool** reset to _HOME_ mode

#### oledRefresh()
repaint the OLED display if is has been marked _dirty_ explicily or by other operations

#### oledUpdateIcons()
update the icons immediately to their new state

state values are:
  - OLED_ICON_STATE_OFF: turn the icon off
  - OLED_ICON_STATE_ON: turn the icon on
  - OLED_ICON_STATE_UNK: do not change the icon
 
input:
    - RX_state **int8_t** RX icon
    - TX_state **int8_t** TX icon
    - CON_state **int8_t** Remote client connected

NOTE: the icons are refreshed immediately using a partial refresh of just the icon area

#### oledPrint()
append a line to the OLED text buffer and display it immediately, scrolling it into view as needed

 - input:
     - msg **char ptr** message to output
	 - wrap **bool** wrap long lines


#### oledPrint()
append a `printf()` style line to the OLED text buffer and display it immediately, scrolling it into view as needed

  - input: id **char ptr** printf style format for the message to output; followed by the arg_list

#### oledButtonState()
Return the current button state

The range of button states allows the caller to track both the start of a press event or long press as well as the eventual release.

 The button states are:
  - IDLE - the button has no interaction
  - PRESS - the button has been pressed
  - LONG PRESS - the button has been pressed and held down for more than 750 ms
  - RELEASE - the button has been released after being pressed
  - LONG RELEASE - the button has been released after being held for more than 750 ms
  - UNKNOWN - something went wrong ... probably because the programmer of this code didn't have enough espresso and was asleep at the wheel

**Note**: This is a _polling function, not interrupt driven_

 - return: **uint8_t** one of the button state defined values

#### oledWaitForButton()
Wait for a button press/release before continuing

The button state is ignored until it is IDLE then waits for the button release event. The function only returns once the button RELEASE has been detected.

**Note**: This is a _polling function, not interrupt driven_ and this is a _blocking operation_.

--------------------------------------------------------------------------
### Web API
Provides the web server and websockets interfaces.

The web files are stored in SPIFFS. The files may be uploaded either as an spiffs.bin or
individual files may be uploaded using the web interface: `http://webisp.local/upload.html`

The UI is rendered as HTML with JavaScript and sends JSON messages through the web socket interface.

The web sockets interface may be extended to add additional events and interactions.
Add new commands to `web_socket_event()` within the section for `WStype_TEXT`.

#### webTelnetPrint()
if a telnet client is active, send the message and a newline

#### webInit()
Performs all necessary initialization of both the webserver on port 80 and the websockets interface on port 81.
Must be called once before using any of the other web functions.
 - return: **bool** `true` on success and `false` on failure

#### webLoop()
Give the web interfaces an opportunity to respond to page requests and websockets messages

--------------------------------------------------------------------------
### WIFI API
Provides wifi or hotspot access and enables over-the-air updates.

#### wifiInit()
Performs all necessary initialization. Must be called once before using any of the other wifi functions.
 - return: **bool** `true` on success and `false` on failure

#### wifiLoop()
Give the WiFi services an opportunity to respond to any necessary actions

#### wifiOff()
Turn the WiFi off; it saves the current state

#### wifiOn()
Turn the WiFi off; it saves the current state

#### wifiIsConnected()
get the current availability of wifi connectivity - either via an existing access point or as a hotspot
 - return: **bool** `true` when wifi is available and `false` when not

#### wifiConnectionLost()
test is we still have wifi connectivity; if we never had wifi we can't lose something we never had
 - return: **bool** `true` if we lost a prior wifi connection and `false` if we are at the same state as before

#### wifiIsHotspot()
indicate if the wifi connection is a hotspot or connected to an access point
 - return: **bool** `true` when wifi is a hotspot and `false` when it is an access point

#### wifiAddress()
a convenience function to get the WiFi address as character string
 - return: **char* ** static internal buffer of the wifi address in the form nnn.nnn.nnn.nnn

#### wifiAddressEnding()
a convenience function to get the last value in the WiFi address
 - return: **uint8_t** last number in wifi address eg xxx.xxx.xxx.NNN

#### otaInit()
Performs all necessary initialization. Must be called once before using any of the other OAT functions.
 - return: **bool** `true` on success and `false` on failure

#### otaLoop()
Give the OTA an opportunity to respond to over-the-air update requests


--------------------------------------------------------------------------
### FILESYS API
Provide access to the SIFFS file system. Used for web content as well as the integrated logging system.

In addition to static web content, the API also provides interfaces for upload of new content via a web interface.
*The web upload capability requires the requisite web interface be implemented in `web.cpp`.*

--------------------------------------------------------------------------
### ESP32 GPIO pin assignments

The WS2812 RGB LEDs require one output pin.
The HSPI requires four pins, three of which are fixed and one may be changed

*The ESP32 has more pins than an ESP8266. I'm not going ot bother to outline them all here.*

The second USART is used to interface to the AVR.

The **LilyGO TTGO ESP32 w/ 18650 Charging & 0.96" OLED** connects the BOOT button to GPIO0.

The OLED uses I2C on the default pins.

The ESP32's second UART is used to interface to the AVR.

#### MESSAGE(...)
Macro to sending `printf()` style content to both the OLED and Serial console.

