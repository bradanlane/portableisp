/* ***************************************************************************
* File:    avrisp.cpp
* Date:    2018.02.16
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined
* under the MIT License
*
* ******************************************************************************/

/* ---
--------------------------------------------------------------------------
### ISP API for AVR chips
Provides the interface for flashing AVR chips - ATtiny85, ATmega328P, etc - using ISP

There are two methods of using the ISP Programmer:
  - a network addressable programmer via TCP usable with software such as `avrdude`
  - a self contained programmer using files located in the SPIFFS file system

Example TCP usage: `avrdude -b 230400 -c arduino -p t85 -P net:<address>:<port> -U flash:w:default.hex`

_The current `IP address` and `port` are displayed on the OLED during the startup process._

The SPIFFS flash interface is handled by the combination of the OLED display and the UI button.
The display starts with the list of available files.
A short press of the button will step through the list of available files.
A long press will initiation the flash process for the currently highlighted file.
The flash process will display progress messages on the OLED.
A final press of the button returns to the file list.
--- */

#include "filesys.h"
#include "oled.h"
#include "pins.h"
#include "wifi.h"

// ------------------------------------------------------------------------------
// include the TCP and SPIFFS file programmer code
// ------------------------------------------------------------------------------
#include "avrispmem.inc"
#include "avrisptcp.inc"

bool g_file_selected = false;
char g_filename[MAX_FILENAME_LEN];

/* ---
#### ispInit()
Performs all necessary initialization of ISP programming systems.
Must be called once before using any of the other ISP operations.
 - return: **bool** `true` on success and `false` on failure
--- */
bool ispInit() {
	avrisptcp_init();
	avrispmem_init();

	g_file_selected = false;
	g_filename[0] = 0;
	return true;
}

/* ---
#### ispLoop()
Give the ISP processes an opportunity to respond to events
--- */
void ispLoop() {
	avrisptcp_check_state_change();

	if (g_file_selected) {
		avrispmem_flash_file(g_filename);
		// reset file processing
		g_file_selected = false;
		g_filename[0] = 0;

		oledWaitForButton(true);
	}
}

/* ---
#### ispFlashFile()
mark an SPIFFS file to be flash

Note: This function does not perform the flash operation. 
It only saves the file name. 
The flash operation will be handled during the next loop. 
Also, this is not a queuing operation. 
If a file name is already waiting to be flashed, this will usurp.
--- */
void ispFlashFile(const char *name) {
	// hard coded test
	oledClear(false);
	strcpy(g_filename, name);
	g_file_selected = true;
}
