#ifndef __AVR_ISP_H
#define __AVR_ISP_H

bool ispInit();
void ispLoop();

void ispFlashFile(const char *name);

#endif
