/* ***************************************************************************
* File:    avrispmem.inc
* Date:    2019.09.16
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined
* under the MIT License
*
* ******************************************************************************/

/* ---
--------------------------------------------------------------------------
### AVR ISP SPIFFS Interface

Provides AVR flashing using standard HEX files stored on SPIFFS file system
--- */

/*
Pedigree:
    ESP32 as ISP Programmer
    Copyright (c) 2019 Bradan Lane Studio

	Standalone AVR ISP programmer
	August 2011 by Limor Fried / Ladyada / Adafruit
	Jan 2011 by Bill Westfield ("WestfW")

	based on AVRISP (attribution not specified)
*/

#include <SPI.h>

// local includes
#include "pins.h"
#include "filesys.h"
#include "oled.h"

// -------------------------------------------------------------------------
// --------------- Data Structures, Constants, and Globals -----------------
// -------------------------------------------------------------------------

#define FUSE_RW_SUPPORT	// WARNING!!! uncommententing this line should only be done by really smart ... or crazy people

// keep one of each of the following pairs of print macros

#define MAX_NAME_LEN 16
#define MAX_PAGE_SIZE 128

// fuses array used in the chip definition structure
#define FUSE_LOW	0  /* Low fuse */
#define FUSE_HIGH	1 /* High fuse */
#define FUSE_EXT	2  /* Extended fuse */
#define FUSE_PROT	3 /* memory protection */

#define SPI_FREQ 125e3

typedef struct chip_definition {
	char name[MAX_NAME_LEN]; // use for logging; not required for programming - eg "atmega168p"
	uint16_t signature;		 // Low two bytes of signature
							 // Fuses set to zero are untouched.
							 // {lock, low, high, extended}
#ifdef FUSE_RW_SUPPORT
	// low, high, extended, locks
	uint8_t defafuses[4]; // factory default fuses (here for reference)
	uint8_t progfuses[4]; // WARNING!!! fuses to set during programming
						  // Fuse verify mask. Any bits set to zero in these values are
						  // ignored while verifying the fuses after writing them.
						  // All (and only) bits that are unused for this atmega chip should be zero
	uint8_t fusemask[4];
#endif
	uint16_t flashsize; // size of chip flash storage
	uint16_t pagesize;  // size of chip page
} chip_definition_t;

const chip_definition_t g_chip_328p = {
	{"ATmega328P"}, // Chip name, only used for serial printing
	0x950F,			// Signature bytes for 328P
#ifdef FUSE_RW_SUPPORT
	{0x62, 0xD9, 0xFF, 0xFF}, // factory defaults
	{0xE2, 0xDF, 0xFF, 0xFF}, // *MY* normal fuses 0xD7 will preserve EEPROM while 0xDF will erase EEPROM
	{0xFF, 0xFF, 0x07, 0x3F}, // fuse mask
#endif
	32768, // size of chip flash in bytes
	128	// size in bytes of a flash page
};

const chip_definition_t g_chip_328pb = {
	{"ATmega328PB"}, // Chip name, only used for serial printing
	0x9516,			// Signature bytes for 328PB
#ifdef FUSE_RW_SUPPORT
	{0x62, 0xD9, 0xFF, 0xFF}, // factory defaults
	{0xE2, 0xDF, 0xFF, 0xFF}, // *MY* normal fuses 0xD7 will preserve EEPROM while 0xDF will erase EEPROM
	{0xFF, 0xFF, 0x07, 0x3F}, // fuse mask
#endif
	32768, // size of chip flash in bytes
	128	// size in bytes of a flash page
};

const chip_definition_t g_chip_168 = {
	{"atmega168P"}, // Chip name, only used for serial printing
	0x940B,			// Signature bytes for 328P
#ifdef FUSE_RW_SUPPORT
	{0x62, 0xD9, 0xFF, 0xFF}, // factory defaults
	{0xE2, 0xDF, 0xFF, 0xFF}, // *MY* normal fuses 0xD7 will preserve EEPROM while 0xDF will erase EEPROM
	{0xFF, 0xFF, 0x07, 0x3F}, // fuse mask
#endif
	16384, // size of chip flash in bytes
	128	// size in bytes of flash page
};

const chip_definition_t g_chip_t85 = {
	{"attiny85"}, // Chip name, only used for serial printing
	0x930B,		  // Signature bytes
#ifdef FUSE_RW_SUPPORT
	{0x62, 0xDF, 0xFF, 0xFF}, // factory defaults
	{0xE2, 0xD7, 0xFF, 0xFF},											   // *MY* normal fuses
	{0x00, 0x00, 0x00, 0x00},											   // fuse mask
#endif
	8192, // size of chip flash in bytes
	64	// size in bytes of flash page
};

const chip_definition_t g_chip_t412 = {
	{"attiny412"}, // Chip name, only used for serial printing
	0x9223,		  // Signature bytes
#ifdef FUSE_RW_SUPPORT
	{0x00, 0x00, 0x00, 0x00}, // factory defaults
	{0x00, 0x00, 0x00, 0x00},											   // *MY* normal fuses
	{0x00, 0x00, 0x00, 0x00},											   // fuse mask
#endif
	4096, // size of chip flash in bytes
	64	  // size in bytes of flash page
};


const chip_definition_t g_chip_t13a = {
	{"attiny13a"}, // Chip name, only used for serial printing
	0x9007,		  // Signature bytes
#ifdef FUSE_RW_SUPPORT
	{0x6A, 0xFF, 0xFF, 0xFF}, // factory defaults
	{0x22, 0xDF, 0xFF, 0x3F},											   // *MY* normal fuses
	{0x00, 0x00, 0x00, 0x00},											   // fuse mask
#endif
	1024, // size of chip flash in bytes
	32	  // size in bytes of flash page
};


const chip_definition_t *g_chip_definitions[] = {
	&g_chip_168,
	&g_chip_328p,
	&g_chip_328pb,
	&g_chip_t412,
	&g_chip_t13a,
	&g_chip_t85};
uint8_t NUMIMAGES = sizeof(g_chip_definitions) / sizeof(g_chip_definitions[0]);

// Global Variables
static int g_pmode = 0;
static uint8_t g_page_buffer[MAX_PAGE_SIZE]; /* One page of flash */

//static SPISettings g_spi_fuses_settings = SPISettings(100000, MSBFIRST, SPI_MODE0);
//static SPISettings g_spi_flash_settings = SPISettings(1000000, MSBFIRST, SPI_MODE0);
static SPISettings g_spi_flash_settings = SPISettings(SPI_FREQ, MSBFIRST, SPI_MODE0);
//static SPISettings g_spi_fuses_settings = SPISettings(SPI_FREQ, MSBFIRST, SPI_MODE0);
#define g_spi_fuses_settings g_spi_flash_settings	// we may use the same settings for both

// -------------------------------------------------------------------------
// ---------------------- HELPER Functions ---------------------------------
// -------------------------------------------------------------------------

#define HEX_ERROR 0xF
// Turn a Hex digit (0..9, A..F) into the equivalent binary value (0-16); a return of 255 is an error
static uint8_t hex_to_num(uint8_t h) {
	if (h >= '0' && h <= '9')
		return (h - '0');
	if (h >= 'A' && h <= 'F')
		return ((h - 'A') + 10);
	if (h >= 'a' && h <= 'f')
		return ((h - 'a') + 10);

	MESSAGE("Bad digit! 0x%X", h);
	return (HEX_ERROR); // error
}

// a little helper routine for reading HEX values as ascii from a file and converting them to numerical representation - eg '4' 'B' becomes 0x4B
static uint8_t read_hex_byte_from_file(File f) {
	uint8_t b;
	b = hex_to_num(f.read());
	b = (b << 4) + hex_to_num(f.read());
	return b;
}

// a little helper file to detect various EOL signatures
bool is_EOL(char c) {
	if (c == '\n' || c == '\r')
		return true;
	return false;
}

// a little pre-processing of HEX file lines to skip to the real data
bool skip_line_preamble(File f) {
	int c;
	// firmware hex file format consists of a series of line. each line of data starts with zero or more whitespace and then a colon ':'.

	// Strip leading whitespace
	do {
		c = f.read();
	} while (c == ' ' || c == '\n' || c == '\r' || c == '\t');

	if (c != ':') {
		if (c >= 0)
			VERBOSE("Line missing colon at start\n");
		return false;
	}
	return true;
}

// ISP programming of an AVR
static uint16_t spi_transaction(uint8_t a, uint8_t b, uint8_t c, uint8_t d) {
	uint8_t n, m, r;
	SPI.transfer(a);
	n = SPI.transfer(b);
	//if (n != a) error = -1;
	m = SPI.transfer(c);
	r = SPI.transfer(d);

	VERBOSE("SPI write: 0x%X 0x%X 0x%X 0x%X  SPI receive: 0x%X 0x%X 0x%X\n", a, b, c, d, n, m, r);

	return 0xFFFFFF & (((uint32_t)n << 16) + (m << 8) + r);
}

// Simply polls the chip until it is not busy any more - for erasing and programming
static void busy_wait(void) {
	SPI.beginTransaction(g_spi_fuses_settings);
	byte busybit;
	do {
		busybit = spi_transaction(0xF0, 0x0, 0x0, 0x0);
	} while (busybit & 0x01);
	SPI.endTransaction();
}

// -------------------------------------------------------------------------
// ------------------- AVR Fuses and Lock Bits Functions -------------------
// -------------------------------------------------------------------------

// get the current chip signature and lookup its settings
static const chip_definition_t *find_chip() {
	uint16_t signature = 0;

	// read the bottom two signature bytes; the highest signature byte is the same over all AVRs so we skip it

	//SPI.beginTransaction(g_spi_fuses_settings);
	signature = spi_transaction(0x30, 0x00, 0x01, 0x00);
	signature <<= 8;
	signature |= spi_transaction(0x30, 0x00, 0x02, 0x00);
	//SPI.endTransaction();

	MESSAGE("Chip signature: 0x%04X\n", signature);

	if (signature == 0) {
		MESSAGE("  (no target attached?)\n");
		return 0;
	}
	if (signature == 0xFFFF) {
		MESSAGE("  (target unknown?)\n");
		return 0;
	}

	const chip_definition_t *cp;
	// MESSAGE("Searching for chip:");

	for (uint8_t i = 0; i < NUMIMAGES; i++) {
		cp = g_chip_definitions[i];
		if (cp && (cp->signature == signature)) {
			MESSAGE("Chip: %s\n", cp->name);
			return cp;
		}
	}

	MESSAGE("Chip Not Found\n");
	return 0;
}

#ifdef FUSE_RW_SUPPORT // we don't want to allow changing of fuses and lock bits until we can trust ourselves
// Program one fuse
static void program_fuse_byte(uint8_t fuse_byte, uint8_t val, const char *name) {
	uint8_t readfuse;
	busy_wait();
	if (val) {
		SPI.beginTransaction(g_spi_fuses_settings);
		readfuse = spi_transaction(0xAC, fuse_byte, 0x00, val);
		SPI.endTransaction();
		Serial.print("  ");
		Serial.print(name);
		Serial.print(" Fuse: ");
		Serial.print(fuse_byte, HEX);
		Serial.print(":");
		Serial.print(val, HEX);
		Serial.print(" -> ");
		Serial.print(readfuse, HEX);
	}
}

// Program the fuse/lock bits
static bool program_fuses(const uint8_t *fuses) {

	Serial.println("Setting fuses:");

	//Serial.println("WARNING!! this is so dangerous, it's disabled");
	//return true;

	program_fuse_byte(0xA0, fuses[FUSE_LOW], "Low");
	program_fuse_byte(0xA8, fuses[FUSE_HIGH], "High");
	program_fuse_byte(0xA4, fuses[FUSE_EXT], "Ext");
	program_fuse_byte(0xE0, fuses[FUSE_PROT], "Lock");

	Serial.println();
	return true;
}
#endif

static uint8_t read_fuse_byte(uint8_t byte1, uint8_t byte2, const char *name) {
	uint8_t readfuse;

	SPI.beginTransaction(g_spi_fuses_settings);
	readfuse = spi_transaction(byte1, byte2, 0x00, 0x00); // lock fuse
	SPI.endTransaction();
	VERBOSE("  %s Fuse: %X", name, readfuse);

	return readfuse;
}

// read the fuse set
static void read_fuses() {
	uint8_t l, h, x, p;

	//MESSAGE("Reading fuses:");

	l = read_fuse_byte(0x50, 0x00, "Low");
	h = read_fuse_byte(0x58, 0x08, "High");
	x = read_fuse_byte(0x50, 0x08, "Ext");
	p = read_fuse_byte(0x58, 0x00, "Lock");

	VERBOSE("\n");

	MESSAGE("Fuses: L%2X H%2X X%2X P%2X\n", l, h, x, p);
}

#ifdef FUSE_RW_SUPPORT // we don't want to allow changing of fuses and lock bits until we can trust ourselves
static bool verify_fuse_byte(uint8_t byte1, uint8_t byte2, uint8_t val, uint8_t mask, const char *name) {
	if (val) {
		uint8_t b = read_fuse_byte(byte1, byte2, name);
		b &= mask;
		if (b != val)
			return 0;
	}
	return true;
}

// Verify the fuse set
static bool verify_fuses(const uint8_t *fuses, const uint8_t *fusemask) {
	uint8_t successes = 0;
	uint8_t f;

	Serial.println("Verifying fuses:");

	successes += verify_fuse_byte(0x50, 0x00, fuses[FUSE_LOW], fusemask[FUSE_LOW], "Low");
	successes += verify_fuse_byte(0x58, 0x08, fuses[FUSE_HIGH], fusemask[FUSE_HIGH], "High");
	successes += verify_fuse_byte(0x50, 0x08, fuses[FUSE_EXT], fusemask[FUSE_EXT], "Ext");
	successes += verify_fuse_byte(0x58, 0x00, fuses[FUSE_PROT], fusemask[FUSE_PROT], "Lock");

	Serial.println();

	if (successes == 4) {
		Serial.println("Fuses verified");
		return true;
	}
	Serial.println("Failed to verify fuses");
	return false;
}
#endif

// -------------------------------------------------------------------------
// -------------------------- AVR Flash Functions --------------------------
// -------------------------------------------------------------------------

// Send the erase command, then busy wait until the chip is erased

void pmode_start() {
	SPI.begin(PIN_ISP_CLK, PIN_ISP_MISO, PIN_ISP_MOSI);
	SPI.setBitOrder(MSBFIRST); // LSBFIRST
	SPI.setFrequency(SPI_FREQ);
	SPI.setHwCs(false);

	// try to sync the bus
	SPI.transfer(0x00);
	digitalWrite(PIN_ISP_RESET, HIGH);
	delayMicroseconds(50);
	digitalWrite(PIN_ISP_RESET, LOW);
	delay(30);

	uint8_t b = spi_transaction(0xAC, 0x53, 0x00, 0x00);
	Serial.printf("\nISPTCP pmode start = 0x%02X\n", b);
	g_pmode = 1;
}

void pmode_end() {
	SPI.end();
	digitalWrite(PIN_ISP_RESET, HIGH);
	g_pmode = 0;
}

static void erase_chip(void) {
	//MESSAGE("Erasing chip:");

	SPI.beginTransaction(g_spi_fuses_settings);
	spi_transaction(0xAC, 0x80, 0, 0); // chip erase
	SPI.endTransaction();
	busy_wait();
	MESSAGE("Chip erased\n");
}

// verifyImage does a byte-by-byte verify of the flash hex against the chip
// Thankfully this does not have to be done by pages!
// returns true if the image is the same as the file, returns false on any error
boolean verify_image(File hex_file) {
	bool success;

	hex_file.seek(0, SeekSet); // reset back to the start of the file
	//MESSAGE("Verifing flash:");

	uint16_t lineaddr;
	uint16_t flashaddr;

	flashaddr = 0;
	lineaddr = 0;

	MESSAGE("Verifying ");

	while (1) {
		char c;
		uint16_t len;
		uint8_t rl, rh, fl, fh, cksum;

		uint16_t tl, th;

		cksum = 0;
		success = false;

		// firmware hex file format:
		// each line of data starts with a preamble consisting of zero or more whitespace and then a colon ':'
		// subsequent characters are text HEX representation of a byte - e.g. '2' and 'F' become 0x2F
		// llaaaarrdd...cc
		// ll = bytes of data on line , aaaa = page address, rr = record type, dd... = remaining data bytes on line, cc = checksum

		// let's begin to read one line ...

		if (!skip_line_preamble(hex_file))
			break;

		// Read the byte count into 'len'
		cksum = (len = read_hex_byte_from_file(hex_file));

		// read high address byte
		cksum += (rh = read_hex_byte_from_file(hex_file));
		// read low address byte
		cksum += (rl = read_hex_byte_from_file(hex_file));
		lineaddr = (rh << 8) + rl;

		// read record type
		cksum += (rl = read_hex_byte_from_file(hex_file));
		//Serial.print("Record type "); Serial.println(b, HEX);

		if (rl == 0x01) {
			// end record, we're done
			success = true;
			break;
		}

		VERBOSE("Flash %0X  Line %0X:\n", flashaddr, lineaddr);
		for (int i = 0; i < len; i += 2) {

			SPI.beginTransaction(g_spi_flash_settings);
			//lb = (spi_transaction(0x20, lineaddr >> 9, lineaddr / 2, 0) & 0xFF); // low byte
			//hb = (spi_transaction(0x28, lineaddr >> 9, lineaddr / 2, 0) & 0xFF); // high bite

			// read byte from file
			cksum += (rl = read_hex_byte_from_file(hex_file));
			cksum += (rh = read_hex_byte_from_file(hex_file));
			th = spi_transaction(0x28, (flashaddr >> 8) & 0xFF, flashaddr & 0xFF, 0);
			tl = spi_transaction(0x20, (flashaddr >> 8) & 0xFF, flashaddr & 0xFF, 0);
			fl = tl & 0xFF;
			fh = th & 0xFF;

			SPI.endTransaction();

			// verify these bytes against bytes from flash
			if (rl != fl) {
				MESSAGE("verror at %0X: LB %0X  %02X != %02X\n", lineaddr, tl, fl, rl);
				break; // return false;
			}
			if (rh != fh) {
				MESSAGE("verror at %0X: HB %0X  %02X != %02X\n", lineaddr, tl, fh, rh);
				break; // return false;
			}
			VERBOSE("%02X:%02X ", rl, rh);
			flashaddr++;
			if ((flashaddr % 256) == 0)
				MESSAGE(".");	// give progress indication
		}
		VERBOSE("\n");

		cksum += (rl = read_hex_byte_from_file(hex_file));
		if (cksum != 0) {
			MESSAGE("Bad checksum on 0x%0X\n", lineaddr);
			break;
		}

		c = hex_file.read();
		if (!is_EOL(c)) {
			VERBOSE("Expected EOL (found %02X) after 0x%X\n", c, lineaddr);
			//break;
		}
	}

	if (success) {
		MESSAGE("\nFlash verified\n");
		return true;
	}
	return false;
}

// Send one byte to the page buffer on the chip
static void write_byte_to_chip(uint8_t hilo, uint16_t addr, uint8_t data) {
	uint8_t val;
	val = spi_transaction(0x40 + (8 * hilo), addr >> 8 & 0xFF, addr & 0xFF, data);
	VERBOSE("%02X:%02X ", data, val);
}

// write the pagebuff (with pagesize bytes in it) into page $pageaddr
static bool write_page_buffer_to_chip(uint16_t pageaddr, uint8_t pagesize, uint8_t *pagebuff) {
	uint16_t commitreply;

	pageaddr = (pageaddr / 2); // page addr is in bytes, but we need to convert to words (/2)

	VERBOSE("Flashing page 0x0%X\n", pageaddr);

	SPI.beginTransaction(g_spi_flash_settings);

	for (uint16_t i = 0; i < pagesize; i+=2) {
		write_byte_to_chip(LOW, pageaddr+(i/2), pagebuff[i]);
		write_byte_to_chip(HIGH, pageaddr+(i/2), pagebuff[i+1]);
	}

	commitreply = spi_transaction(0x4C, (pageaddr >> 8) & 0xFF, pageaddr & 0xFF, 0);
	SPI.endTransaction();

	VERBOSE("\n");

	VERBOSE("  Commit Page: 0x%x -> 0x%X\n", pageaddr, commitreply);
	if (commitreply != pageaddr) {
		MESSAGE("cerror page: 0x%x -> 0x%X\n", pageaddr, commitreply);
		return false;
	}

	busy_wait();
	return true;
}

// Read a page of intel hex image from file; return number of bytes decoded
static uint8_t read_file_to_page_buffer(File hex_file, uint8_t pagesize, uint8_t *page_buffer) {
	// persistant variables
	static uint8_t cksum = 0;
	static uint16_t len = 0;
	static bool new_page = true;
	// local variables
	uint8_t index;
	uint8_t b;
	char c;

	// firmware hex file format consists of a series of line. each line of data starts with zero or more whitespace and then a colon ':'.
	// all characters after the colon are text HEX representation of a byte - e.g. '2' and 'F' become 0x2F
	// llaaaarrdd...cc
	// ll = bytes of actual data on line, aaaa = page address, rr = record type, dd... = zero or more data bytes, cc = checksum

	// 'empty' the page by filling it with 0xFF's
	for (uint8_t i = 0; i < pagesize; i++)
		page_buffer[i] = 0xFF;

	index = 0;
	// read bytes from HEX file
	while (index < pagesize) {
		uint16_t lineaddr = 0;

		if (new_page) {
			cksum = 0;
			if (!skip_line_preamble(hex_file))
				break;

			// Read the byte count into 'len'
			cksum = (len = read_hex_byte_from_file(hex_file));

			// read high address byte
			cksum += (b = read_hex_byte_from_file(hex_file));
			lineaddr = b;
			// read low address byte
			cksum += (b = read_hex_byte_from_file(hex_file));
			lineaddr = (lineaddr << 8) + b;

			/*	// check if data will fit in flash space
			if ((lineaddr + len) > (pageaddr + pagesize)) {
				rewind_file(hex_file);
				return index;
				//return 0;
			}
			*/

			cksum += (b = read_hex_byte_from_file(hex_file)); // read record type
			//Serial.print("Record type "); Serial.println(b, HEX);
			if (b == 0x1) { // end record, return index=0 to indicate we're done
				new_page = true;
				len = 0;
				cksum = 0;
				break;
			}

			VERBOSE("Line address: 0x%0X\n", lineaddr);
			new_page = false;
		}

		// read bytes
		cksum += (b = read_hex_byte_from_file(hex_file));
		page_buffer[index] = b;
		index++;
		len--;

		VERBOSE("%02X ", b);

		if (!len) {
			// read chxsum; should zero out our cksum accumulation
			cksum += (b = read_hex_byte_from_file(hex_file));
			if (cksum != 0) {
				MESSAGE("Bad checksum at 0x%X\n", lineaddr);
				break;
			}
			c = hex_file.read();
			if (!is_EOL(c)) {
				//MESSAGE("Expected EOL (found %02X) at address 0x%X\n", c, lineaddr);
				break;
			}
			// reset all persistent variables
			new_page = true;
			len = 0;
			cksum = 0;
		}
	}

	VERBOSE("\nTotal bytes read: %d\n", index);

	return index;
}

static bool write_file_to_chip(char *filename) {
	File hex_file;
	bool success;

	MESSAGE("Flashing: %s\n", filename);

	hex_file = filesysOpen(filename);
	if (!hex_file) {
		MESSAGE("  File not found\n");
		return false;
	}

	success = true; // assume we are successful; we change this at any point that we know we failed

	pmode_start();

	do { // a do {...} while(0); loop allows use to break to teh end at any point
		const chip_definition_t *target_chip;
		if (!(target_chip = find_chip())) {
			success = false;
			break;
		}

		uint8_t pagesize = target_chip->pagesize;
		uint16_t chipsize = target_chip->flashsize;
		uint16_t pageaddr = 0;

		if (pagesize > MAX_PAGE_SIZE) {
			MESSAGE("ERROR: pagesize %d > %d\n", pagesize, MAX_PAGE_SIZE);
			success = false;
			break;
		}

		MESSAGE("Flash: %d Page: %d\n", chipsize, pagesize);

#ifdef FUSE_RW_SUPPORT
		program_fuses(target_chip->progfuses);
#endif
		erase_chip();

		read_fuses(); // for informational purposes only

#if 0
		// cycle chip state for programming
		pmode_end();
		pmode_start(false);
#endif

		//MESSAGE("Starting Flash:");
		pageaddr = 0;
		while (pageaddr < chipsize) {
			VERBOSE("Writing address %X\n", pageaddr);

			read_file_to_page_buffer(hex_file, pagesize, g_page_buffer);

			// test if entire buffer is all filler bytes aka 0xFF
			bool blankpage = true;
			for (uint8_t i = 0; i < pagesize; i++) {
				if (g_page_buffer[i] != 0xFF)
					blankpage = false;
			}
			if (!blankpage) {
				if (!write_page_buffer_to_chip(pageaddr, pagesize, g_page_buffer)) {
					MESSAGE("Flash programming failed\n");
					success = false;
					break;
				}
			} else {
				// skip blank pages; typically this occurs at end of flash file since its rare to use 100% of flash storage
			}
			pageaddr += pagesize;

			if ((pageaddr % 256) == 0) // assume pagesize is an integer divisor of 256
				MESSAGE(".");	// give progress indication
		}

		if (success)
			MESSAGE("\nFlash finished\n");

		delay(100);
#if 0
		pmode_end();
		delay(100);
		pmode_start(false);
		delay(100);
#endif
		// technically, if pageaddr < chipsize, then something went wrong and we stopped early
		// if we stopped early, there realyl is no good reason to perform the verification
		// TODO check the above assumption and adjust code accordingly

		if (success) {
			success = verify_image(hex_file);
		}

	} while (0); // this do-while non-loop allows us to break out at any point we have an error

	pmode_end();
	filesysClose(hex_file);
	return success;
}

// -------------------------------------------------------------------------
// ----------------------------- Public-like Functions --------------------------
// -------------------------------------------------------------------------

static bool avrispmem_flash_file(char *filename) {
	// just in case teh device was attached since we started ...
	pinMode(PIN_ISP_RESET, OUTPUT);
	digitalWrite(PIN_ISP_RESET, HIGH);

	return write_file_to_chip(filename);
}

static bool avrispmem_init() {
	// perform any required initialization for ISP Programming from SPIFFS
	pinMode(PIN_ISP_RESET, OUTPUT);
	digitalWrite(PIN_ISP_RESET, HIGH);

	pmode_start();
	const chip_definition_t *target_chip;
	if (!(target_chip = find_chip())) {
		VERBOSE("chip not found\n");
	}
	else {
		VERBOSE("Chip(%04X) is %s\n", target_chip->signature, target_chip->name);
	}
	pmode_end();

	return true;
}
