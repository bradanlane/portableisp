/* ***************************************************************************
* File:    filesys.cpp
* Date:    2019.09.09
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined
* under the MIT License
*
* ******************************************************************************/

/* ---

--------------------------------------------------------------------------
### FILESYS API
Provide access to the SIFFS file system. Used for web content as well as the integrated logging system.

In addition to static web content, the API also provides interfaces for upload of new content via a web interface.
*The web upload capability requires the requisite web interface be implemented in `web.cpp`.*
--- */




#include <FS.h>
#include <SPIFFS.h>

// local includes
#include "filesys.h"

static File _filesys_upload_file; // a File variable to temporarily store the received file
static int _filesys_upload_size = 0;
static File _spiffs_dir; // the directory list

static char *formatBytes(size_t bytes) { // convert sizes in bytes to KB and MB
	static char fmt_buf[MAX_FORMATBYTES + 1];
	if (bytes < 1024)
		snprintf(fmt_buf, MAX_FORMATBYTES, "%4d Bs", bytes);
	else if (bytes < (1024 * 1024))
		snprintf(fmt_buf, MAX_FORMATBYTES, "%6.1f KB", (bytes / 1024.0));
	else if (bytes < (1024 * 1024 * 1024))
		snprintf(fmt_buf, MAX_FORMATBYTES, "%6.1f MB", (bytes / 1024.0 / 1024.0));
	else
		snprintf(fmt_buf, MAX_FORMATBYTES, "%6.1f GB", (bytes / 1024.0 / 1024.0 / 1024.0));

	fmt_buf[MAX_FORMATBYTES] = 0;
	return fmt_buf;
}

bool filesysInit() { // Start the SPIFFS and list all contents
	bool success = false;
	// attempt to start
	success = SPIFFS.begin();
	if (!success)
		success = SPIFFS.begin(true); // attempt to format SPIFFS

	if (success) { // Start the SPI Flash File System (SPIFFS)
		_spiffs_dir = SPIFFS.open("/");

		Serial.println("SPIFFS started. Contents:");
		char *info = filesysGetFileInfo(true);
		while (info) {
			Serial.printf("\t%s\n", info);
			info = filesysGetFileInfo(false);
		}
		Serial.printf("\n");
		return true;
	}
	Serial.println("SPIFFS failed.");
	return false;
}

void filesysLoop() {
	// currently nothing to do
}

bool filesysSaveStart(const char* name) {
	char filename[MAX_FILENAME_LEN + 1];
	if (name[0] != '/')
		snprintf(filename, MAX_FILENAME_LEN, "/%s", name);
	else
		snprintf(filename, MAX_FILENAME_LEN, "%s", name);

	_filesys_upload_size = 0;
	Serial.printf("handleFileUpload Name: %s\n", filename);
	_filesys_upload_file = SPIFFS.open(filename, "w"); // Open the file for writing in SPIFFS (create if it doesn't exist)

	if (_filesys_upload_file)
		return false;
	return true;
}

bool filesysSaveWrite(uint8_t *buf, size_t size) {
	if (_filesys_upload_file) {
		_filesys_upload_file.write(buf, size); // Write the received bytes to the file
		_filesys_upload_size += size;
	}
	return true; // TODO check return code and/or byte count and return appropriate status
}

bool filesysSaveFinish() {
	if (_filesys_upload_file) {
		_filesys_upload_file.close(); // Close the file again
		Serial.printf("Upload %d bytes\n", _filesys_upload_size);
		_filesys_upload_size = 0;
		return true;
	}
	return false;
}

bool filesysExists(const char* name) {
	return SPIFFS.exists(name);
}

void filesysDelete(const char* name) {
	if (name && *name)
		SPIFFS.remove(name);
}

File filesysOpen(const char* name) {
	File f = SPIFFS.open(name, FILE_READ); // Open the file
	if (!f)
		Serial.printf("Failed to open %s for read\n", name);
	return f;
	//return SPIFFS.open(name, FILE_READ); // Open the file
}

void filesysClose(File handle) {
	handle.close();
}

char *filesysGetFileInfo(bool first) {
	static char buf[MAX_FILENAME_LEN + MAX_FORMATBYTES + 1];
	File file;

	buf[0] = 0;
	if (first) {
		_spiffs_dir.rewindDirectory();
		file = _spiffs_dir.openNextFile();
	} else
		file = _spiffs_dir.openNextFile();

	if (file) {
		if (file.isDirectory()) {
			snprintf(buf, (MAX_FILENAME_LEN + MAX_FORMATBYTES), "[%s]", file.name());
		} else
			snprintf(buf, (MAX_FILENAME_LEN + MAX_FORMATBYTES), "%s\t%s", file.name(), formatBytes(file.size()));
	}
	if (buf[0])
		return buf;
	return 0;
}
