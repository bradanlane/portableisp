#ifndef __FILE_SYSTEM_H
#define __FILE_SYSTEM_H

#include <FS.h>

#define MAX_FILENAME_LEN 32
#define MAX_FORMATBYTES 10

bool filesysInit();
void filesysLoop();

bool filesysExists(const char* name);
void filesysDelete(const char *name);
File filesysOpen(const char *name);
void filesysClose(File handle);
char *filesysGetFileInfo(bool first);

bool filesysSaveStart(const char* name);
bool filesysSaveWrite(uint8_t* buf, size_t size);
bool filesysSaveFinish();

#endif
