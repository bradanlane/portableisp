/* ***************************************************************************
* File:    main.cpp
* Date:    2019.09.09
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined
* under the MIT License
*
* ******************************************************************************/

/* ---
# PortableISP

**NOTE:** THIS PROJECT IS NO LONGER MAINTAINS AND HAS BEEN SUPERSEDED BY THE PORTAPROG


An AVR ISP Programmer and UART Terminal interface built with an ESP32.

IMPORTANT: This project was developed using the **LilyGO TTGO ESP32 w/ 18650 Charging & 0.96" OLED** board and
takes advantage of many of its features including the on-board OLED and the BOOT button.

Supports direct firmware flashing over TCP and reflashing using firmware stored on the SPIFFS.

Supports telnet for remote UART/Serial terminal

**TODO:**
  - include a simple web browser interface as a UART/Serial terminal
  - add SSL support for browser connections to the web server and websocket interface

![Portable ISP Programmer](https://gitlab.com/bradanlane/portableisp/raw/master/files/portableisp_small.jpg)

--------------------------------------------------------------------------
--- */

#include <HardwareSerial.h>

// local includes

#include "pins.h"
#include "filesys.h"
#include "wifi.h"
#include "web.h"
#include "avrisp.h"
#include "oled.h"

#include <functional>


// -----------------------------------------------------------
// the standard Arduino entrypoints setup() and loop()
// -----------------------------------------------------------

void setup() {
	Serial.begin(115200);
	while (!Serial)
		; // wait for serial attach
	Serial.println("");
	Serial.println("");
	Serial2.begin(9600, SERIAL_8N1, PIN_USART_RX, PIN_USART_TX);
	while (!Serial2)
		; // wait for serial attach
	Serial2.println("");
	Serial2.println("");

	oledInit();
	filesysInit();
	wifiInit();
	otaInit();		// used wifi
	webInit();
	ispInit();
	Serial.println("---- System Initialized ----");
	// delay(2000);
	oledWaitForButton(true);
}

void loop() {
	wifiLoop();
	otaLoop();
	filesysLoop();
	webLoop();
	ispLoop();
	oledLoop();
	delay(1);
}
