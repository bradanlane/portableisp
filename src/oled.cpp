/* ***************************************************************************
* File:    oled.cpp
* Date:    2019.09.16
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined
* under the MIT License
*
* ******************************************************************************/

/* ---
--------------------------------------------------------------------------
### OLED API
Provides the display and button UI using and SSD1306 I2C OLED display module

The display and UI interface supports text-line based display functions.

The UI consists of a single button which reports its current state.

There are three display modes:
  - file list - a list of the files currently in SPIFFS storage
  - file menu - list of options available fore the currently selected file
  - text msg - a scrolling screen of messages for the current operation

--- */

#include <Arduino.h>
#include <U8g2lib.h>

// now the local includes
#include "avrisp.h"
#include "pins.h"
#include "filesys.h"
#include "oled.h"
#include "web.h"

// The size (width * height) depends on the display and font
#define HEADER_FONT u8g2_font_7x13B_mr
#define WINDOW_FONT u8g2_font_5x8_mr
#define FONT_WIDTH 5
#define FONT_HEIGHT 8
#define MAX_CHARS 25	 // screen width divided by font width
#define MAX_LINES 8		 // number of lines using are normal font
#define MAX_LIST_LINES 6 // number of lines after the header
#define MAX_TEXT 256	 // number of characters we will allow for a wrapping line of text

static char g_lines_buffer[MAX_LINES][MAX_CHARS+1];
static uint8_t g_current_line; // number of lines in the buffer currently being used
static uint8_t g_current_char;
static uint8_t g_file_list_length; // total count of files; updated by file_list_display()
static uint8_t g_skipped_lines;	// number of files from g_file_list_length which have scrolled off the top; updated by file_list_display()
static uint8_t g_file_list_index;  // the currently highlighted position on the display; incremented by the button

static bool g_display_dirty;
#define SCROLL_OFFSET 2 // number of lines from botton to start scrolling

#define OLED_MODE_FILES 0
#define OLED_MODE_TEXT 1
#define OLED_MODE_SERIAL 2
#define OLED_MODE_MENU 3
#define OLED_MODE_HOME OLED_MODE_FILES
static uint8_t g_oled_mode;

#define OLED_FILE_MENU_FLASH 0
#define OLED_FILE_MENU_DELETE 1
#define OLED_FILE_MENU_RETURN 2
static char g_selected_filename[MAX_FILENAME_LEN + 1];
static bool g_flashing;

// the first of these uses only 128 bytes but requies the beginPage()..nextPage() loop
// the second of these uses 1024 bytes and requires clearBuffer() sendBuffer() pre/post calls
//static U8G2_SSD1306_128X64_NONAME_1_HW_I2C g_oled(U8G2_R2, U8X8_PIN_NONE, PIN_I2C_SCL, PIN_I2C_SDA);
//U8G2_SSD1306_128X64_NONAME_F_SW_I2C g_oled(U8G2_R2, PIN_I2C_SCL, PIN_I2C_SDA, U8X8_PIN_NONE);
U8G2_SSD1306_128X64_NONAME_F_HW_I2C g_oled(U8G2_R2, U8X8_PIN_NONE, PIN_I2C_SCL, PIN_I2C_SDA);

static void refresh_text_display() {
	uint8_t y_pos;
	g_oled.clearBuffer();

	g_oled.setFont(WINDOW_FONT);
	g_oled.setFontMode(1);
	g_oled.setDrawColor(2);
	y_pos = FONT_HEIGHT;
	for (int8_t i = 0; i < MAX_LINES; i++) {
		if (g_lines_buffer[i][0]) {
			g_oled.drawStr(0, y_pos, g_lines_buffer[i]);
			//Serial.printf("  g_lines_buffer[%d]: %s\n", i, g_lines_buffer[i]);
		}
		y_pos += FONT_HEIGHT;
	}
	oledUpdateIcons(OLED_ICON_STATE_UNK, OLED_ICON_STATE_UNK, OLED_ICON_STATE_UNK); // repaint icons with their current state
	g_oled.sendBuffer();
	//g_display_dirty = false;
}

static void refresh_file_list_display() {
	const char TITLE[] = "Files";
	char *filename = 0;
	u8g2_uint_t x_pos = 0, y_pos = 0, line_index = 0;

	// cache the file list that will get rendered
	// use g_lines_buffer[lines][characters];

	// the scroll_line is the position on the display we use to trigger scrolling of the list
	// to improve usability, we try to scroll the list when we are in the middle of the display

	g_file_list_length = 0;
	g_skipped_lines = 0;

	filename = filesysGetFileInfo(true);
	for (int8_t i = g_file_list_index - (MAX_LIST_LINES - SCROLL_OFFSET); i >= 0; i--) {
		filename = filesysGetFileInfo(false);
		if (!filename)
			break;
		//Serial.printf("skipping %s\n", filename);
		g_skipped_lines++;
		g_file_list_length++;
	}
	//Serial.printf("max list size = %d  list index = %d  skipped lines = %d\n", MAX_LIST_LINES, g_file_list_index, skipped_lines);

	// now we copy the visible list of files into the display buffer
	for (int8_t i = 0; i < MAX_LIST_LINES; i++) {
		g_lines_buffer[line_index][0] = 0;
		if (filename) {
			g_file_list_length++;

			// if there is a tab then we want to split the string into the name and the size info
			char *pos = strstr(filename, "\t");
			if (pos) {
				*pos++ = 0;
				snprintf(g_lines_buffer[line_index], MAX_CHARS, "%s %*s", filename, ((MAX_CHARS - strlen(filename)) - 2), pos);
			} else
				strncpy(g_lines_buffer[line_index], filename, MAX_CHARS);
			g_lines_buffer[line_index][MAX_CHARS - 1] = 0;
			filename = filesysGetFileInfo(false);
		}
		line_index++;
	}

	// finish counting the files not visible so it is available for the button processing
	while (filename) {
		g_file_list_length++;
		filename = filesysGetFileInfo(false);
	}

	g_oled.clearBuffer();

	// render header title area
	g_oled.setFont(HEADER_FONT);
	y_pos = g_oled.getMaxCharHeight();
	x_pos = (g_oled.getDisplayWidth() - g_oled.getStrWidth(TITLE)) / 2;
	g_oled.drawStr(x_pos, y_pos, TITLE);
	y_pos++;
	g_oled.drawHLine(0, y_pos, g_oled.getDisplayWidth());
	y_pos += 2;

	// render file list with simulated scrolling
	g_oled.setFont(WINDOW_FONT);
	g_oled.setFontMode(1);
	g_oled.setDrawColor(2);
	for (int8_t i = 0; i < MAX_LIST_LINES; i++) {
		if (g_lines_buffer[i][0]) {
			y_pos = (g_oled.getDisplayHeight() - ((MAX_LIST_LINES - i) * FONT_HEIGHT)) + FONT_HEIGHT;
			g_oled.drawStr((1 * FONT_WIDTH), y_pos, g_lines_buffer[i]);
			if (i == (g_file_list_index - g_skipped_lines)) {
				// XOR mode will invert the pixels within the content of the box
				g_oled.drawBox(0, (y_pos - FONT_HEIGHT) + 1, g_oled.getDisplayWidth(), FONT_HEIGHT);
				//g_oled.drawStr(0, y_pos, ">");
				//g_oled.drawStr((g_oled.getDisplayWidth() - FONT_WIDTH), y_pos, "<");
			}
		}
	}
	oledUpdateIcons(OLED_ICON_STATE_UNK, OLED_ICON_STATE_UNK, OLED_ICON_STATE_UNK); // repaint icons with their current state
	g_oled.sendBuffer();

	g_display_dirty = false;
}

static void refresh_file_menu_display() {
	//const char TITLE[] = "File Operations";
	char *TITLE = g_selected_filename;
	u8g2_uint_t x_pos = 0, y_pos = 0;
	uint8_t skipped_lines = 0;

	if (g_selected_filename[0])
		TITLE = &(g_selected_filename[1]); // skip the leading forward slash

	//oledClear();
	for (int8_t i = 0; i < MAX_LIST_LINES; i++)
		g_lines_buffer[i][0] = 0;

	strncpy(g_lines_buffer[OLED_FILE_MENU_FLASH],  "         FLASH          ", MAX_CHARS);
	strncpy(g_lines_buffer[OLED_FILE_MENU_DELETE], "         DELETE         ", MAX_CHARS);
	strncpy(g_lines_buffer[OLED_FILE_MENU_RETURN], "         RETURN         ", MAX_CHARS);
	g_file_list_length = 3;

	g_oled.clearBuffer();

	// render header title area
	g_oled.setFont(HEADER_FONT);
	y_pos = g_oled.getMaxCharHeight();
	x_pos = (g_oled.getDisplayWidth() - g_oled.getStrWidth(TITLE)) / 2;
	g_oled.drawStr(x_pos, y_pos, TITLE);
	y_pos++;
	g_oled.drawHLine(0, y_pos, g_oled.getDisplayWidth());
	y_pos += 2;

	// render file list with simulated scrolling
	g_oled.setFont(WINDOW_FONT);
	g_oled.setFontMode(1);
	g_oled.setDrawColor(2);
	for (int8_t i = 0; i < MAX_LIST_LINES; i++) {
		if (g_lines_buffer[i][0]) {
			y_pos = (g_oled.getDisplayHeight() - ((MAX_LIST_LINES - i) * FONT_HEIGHT)) + FONT_HEIGHT;
			g_oled.drawStr((1 * FONT_WIDTH), y_pos, g_lines_buffer[i]);
			if (i == (g_file_list_index - skipped_lines)) {
				g_oled.drawBox(0, (y_pos - FONT_HEIGHT) + 1, g_oled.getDisplayWidth(), FONT_HEIGHT);
				//g_oled.drawStr(0, y_pos, ">");
				//g_oled.drawStr((g_oled.getDisplayWidth() - FONT_WIDTH), y_pos, "<");
			}
		}
	}
	oledUpdateIcons(OLED_ICON_STATE_UNK, OLED_ICON_STATE_UNK, OLED_ICON_STATE_UNK); // repaint icons with their current state
	g_oled.sendBuffer();

	g_display_dirty = false;
}

// -----------------------------------------------------------------------------------------
// begin of public interfaces
// -----------------------------------------------------------------------------------------

/* ---
#### oledInit()
Performs all necessary initialization of both the OLED and the button(s)
Must be called once before using any of the other display functions.
 - return: **bool** `true` on success and `false` on failure
--- */
bool oledInit(void) {
	g_oled.begin();
	g_oled.setFont(WINDOW_FONT);

	pinMode(PIN_BUTTON, INPUT_PULLUP);
	oledClear(true);
	oledUpdateIcons(OLED_ICON_STATE_OFF, OLED_ICON_STATE_OFF, OLED_ICON_STATE_OFF);
	g_flashing = false;
	g_selected_filename[0] = 0;
	return true;
}

/* ---
#### oledLoop()
Give the OLED and button(s) an opportunity to respond to interaction events and update the display
--- */
void oledLoop(void) {
	static uint8_t old_selector = BUTTON_UNKNOWN;
	uint8_t selector = oledButtonState();

	if (selector != old_selector) {
		if (selector == BUTTON_RELEASE) {
			// move down one line in the list
			g_file_list_index++;
			if (g_file_list_index > ((g_file_list_length - SCROLL_OFFSET) + 1))
				g_file_list_index = 0;
			g_display_dirty = true;
		} else if (selector == BUTTON_LONG_PRESS) {
			// a long press is considered 'select' of the current item in list
			// the currently highlighted entry is (g_file_list_index or (g_file_list_index - (MAX_LIST_LINES - SCROLL_OFFSET)))
			int index = g_file_list_index - g_skipped_lines;

			switch (g_oled_mode) {
				case OLED_MODE_FILES: {
					// to be honest, this code is getting pretty convoluted with OLED needed knowledge of filesys and avrisp
					// a bit kludgy but we assume filenames do not have spaces

					if (index >= MAX_LINES) // insanity check
						memcpy(g_selected_filename, "bad.file", MAX_FILENAME_LEN);
					else {
						memcpy(g_selected_filename, g_lines_buffer[(index)], MAX_FILENAME_LEN);
						g_selected_filename[MAX_FILENAME_LEN] = 0;
					}

					char *pos = strstr(g_selected_filename, " ");
					if (pos) *pos = 0;
					g_file_list_index = 0;
					g_display_dirty = true;
					g_oled_mode = OLED_MODE_MENU;
					//Serial.printf("selected %s\n", g_selected_filename);
				} break;

				case OLED_MODE_MENU: {
					switch (g_file_list_index) {
						case OLED_FILE_MENU_FLASH: {
							g_flashing = true;
							ispFlashFile(g_selected_filename);
						} break;
						case OLED_FILE_MENU_DELETE: {
							filesysDelete(g_selected_filename);
						} break;
						default: {
							//Serial.printf("menu unknown[%d] action on %s\n", g_file_list_index, g_selected_filename);
						} break;
					}
				} // fall thru to default and set mode back to FILES;
				default: {
					// UX experiment: allow system to stay on MENU following a FLASH operation
					if (!g_flashing) {
						g_file_list_index = 0;
						g_oled_mode = OLED_MODE_FILES;
						g_selected_filename[0] = 0;
					}
					g_display_dirty = true;
				}
			}
		}
		// remember most recent button state
		old_selector = selector;
	}

	oledRefresh();
	//delay(1);
}

/* ---
#### oledClear()
delete all current content and mark display buffer as dirty so it is refreshed during the next cycle

- input: **bool** reset to _HOME_ mode
--- */
void oledClear(bool home) {
	for (int i = 0; i < MAX_LINES; i++)
		for (int j = 0; j < (MAX_CHARS + 1); j++)	// lines have an extra byte for a null terminator
			g_lines_buffer[i][j] = 0;
	g_current_line = 0;
	g_current_char = 0;
	g_file_list_index = 0;
	g_file_list_length = 0;
	if (home)
		g_oled_mode = OLED_MODE_HOME;
	g_display_dirty = true;
}

/* ---
#### oledRefresh()
repaint the OLED display if is has been marked _dirty_ explicily or by other operations
--- */
void oledRefresh() {
	// three possible display modes: SPIFFS file list; FLASH Progress; Serial output
	// eventually we will have different things to refresh
	if (g_display_dirty) {
		if (g_oled_mode == OLED_MODE_FILES) {
			refresh_file_list_display();
		} else if (g_oled_mode == OLED_MODE_MENU) {
			refresh_file_menu_display();
		}
	}
}

/* ---
#### oledUpdateIcons()
update the icons immediately to their new state

state values are:
  - OLED_ICON_STATE_OFF: turn the icon off
  - OLED_ICON_STATE_ON: turn the icon on
  - OLED_ICON_STATE_UNK: do not change the icon
 
input:
    - RX_state **int8_t** RX icon
    - TX_state **int8_t** TX icon
    - CON_state **int8_t** Remote client connected

NOTE: the icons are refreshed immediately using a partial refresh of just the icon area
--- */
void oledUpdateIcons(int8_t rx, int8_t tx, int8_t con) {
	static bool rx_icon = false, tx_icon = false, client_icon = false;
	bool dirty = false;
	u8g2_uint_t cx = 0, cy = 0, cd = 0; // circle center x, y, and diameter

#define ICON_AREA_X 104 // assumes the right side of a 128 pixel wide display
#define ICON_AREA_Y 0
#define ICON_AREA_W 23
#define ICON_AREA_H 12
#define ICON_RADIUS (ICON_AREA_H / 2)

	if (rx != OLED_ICON_STATE_UNK) {
		if (rx_icon != rx)
			dirty = true;
		rx_icon = rx;
	}
	if (tx != OLED_ICON_STATE_UNK) {
		if (tx_icon != tx)
			dirty = true;
		tx_icon = tx;
	}
	if (con != OLED_ICON_STATE_UNK) {
		if (client_icon != con)
			dirty = true; // if we ignore this case, then the client connectivity only refreshes on the next change of RX or TX
		client_icon = con;
	}

	if (!dirty)
		return;

	// erase icon area
	g_oled.setDrawColor(0); // erase
	g_oled.drawBox(ICON_AREA_X, ICON_AREA_Y, ICON_AREA_W, ICON_AREA_H);
	g_oled.setDrawColor(1); // solid

	// draw client oval - solid if client connected, else hollow
	cx = ICON_AREA_X + ICON_RADIUS;
	cy = ICON_AREA_Y + ICON_RADIUS;
	cd = ICON_RADIUS;
	if (client_icon) {
		// filled oval is half disc at each end and box in the middle
		g_oled.drawDisc(cx, cy, cd, (U8G2_DRAW_UPPER_LEFT | U8G2_DRAW_LOWER_LEFT));
		cx = (ICON_AREA_X + ICON_AREA_W) - ICON_RADIUS;
		g_oled.drawDisc(cx, cy, cd, (U8G2_DRAW_UPPER_RIGHT | U8G2_DRAW_LOWER_RIGHT));
		g_oled.drawBox(ICON_AREA_X + ICON_RADIUS, ICON_AREA_Y, (ICON_AREA_W - ICON_AREA_H) + 2, ICON_AREA_H);

		g_oled.setDrawColor(0); // // set mode for the TX/RX icons = erase mode
	} else {
		cx = ICON_AREA_X + ICON_RADIUS;
		// filled oval is half disc at each end and box in the middle
		g_oled.drawCircle(cx, cy, cd, (U8G2_DRAW_UPPER_LEFT | U8G2_DRAW_LOWER_LEFT));
		cx = (ICON_AREA_X + ICON_AREA_W) - ICON_RADIUS;
		g_oled.drawCircle(cx, cy, cd, (U8G2_DRAW_UPPER_RIGHT | U8G2_DRAW_LOWER_RIGHT));
		g_oled.drawHLine(ICON_AREA_X + ICON_RADIUS, ICON_AREA_Y, (ICON_AREA_W - ICON_AREA_H) + 1);
		g_oled.drawHLine(ICON_AREA_X + ICON_RADIUS, ICON_AREA_Y + ICON_AREA_H, (ICON_AREA_W - ICON_AREA_H) + 1);

		g_oled.setDrawColor(1); // // set mode for the TX/RX icons = solid mode
	}
	//g_oled.setDrawColor(2); // XOR

	cx = ICON_AREA_X + ICON_RADIUS;
	cy = ICON_AREA_Y + ICON_RADIUS;
	cd = ICON_RADIUS - 2;

	// draw rx circle - solid if active, hollow if not; drawn XOR with background
	if (tx_icon) {
		// filled circle
		g_oled.drawDisc(cx, cy, cd, U8G2_DRAW_ALL);
	} else {
		// hollow circle
		//g_oled.drawCircle(cx, cy, cd, U8G2_DRAW_ALL);
	}

	// draw tx circle - solid if active, hollow if not; drawn XOR with background
	cx = (ICON_AREA_X + ICON_AREA_W) - ICON_RADIUS;
	if (rx_icon) {
		// filled circle
		g_oled.drawDisc(cx, cy, cd, U8G2_DRAW_ALL);
	} else {
		// hollow circle
		//g_oled.drawCircle(cx, cy, cd, U8G2_DRAW_ALL);
	}

	// write display buffer to display
	g_oled.sendBuffer();
	g_oled.setDrawColor(1); // restore solid draw mode
}

/* ---
#### oledPrint()
append a line to the OLED text buffer and display it immediately, scrolling it into view as needed

 - input:
     - msg **char ptr** message to output
	 - wrap **bool** wrap long lines

--- */
void oledPrint(const char *msg, bool wrap) {
	//output the message to serial by default
	Serial.print(msg);

	//webTelnetPrint(msg);

	// mark the display dirty for next refresh cycle
	g_display_dirty = true;
	if (g_oled_mode != OLED_MODE_TEXT) {
		oledClear(false);
		g_oled_mode = OLED_MODE_TEXT;
	}

	// wrap long lines
	char *p = (char *)msg;
	bool wrapped = false;

	while (*p) {
		// if we hit a linefeed newline, wrap
		if ((*p == '\r') || (*p == '\n')) {
			g_lines_buffer[g_current_line][g_current_char] = 0;
			p++; // skip the newline character
			g_current_char = 0;
			// the rest of this starts a new line ... which is technially not correct for '\r'
			g_current_line++;
			wrapped = true;
		}
		else if (g_current_char >= MAX_CHARS) {
			g_lines_buffer[g_current_line][MAX_CHARS] = 0;
			g_current_line++;
			g_current_char = 0;
			wrapped = true;
		}

		// if we wrapped and wrapping lines is allowed, then we keep going
		if (!wrapped || (wrapped & wrap)) {
			if (g_current_line >= MAX_LINES) {
				// scroll to make room
				for (int i = 0; i < (MAX_LINES - 1); i++)
					memcpy(g_lines_buffer[i], g_lines_buffer[i + 1], MAX_CHARS);
				// set current line to the last line in the buffer
				g_current_line = (MAX_LINES - 1);
				// clear the last line buffer
				for (int j = 0; j < (MAX_CHARS + 1); j++)	// lines have an extra byte for a null terminator
					g_lines_buffer[g_current_line][j] = 0;
			}
			if (*p) {
				g_lines_buffer[g_current_line][g_current_char] = *p;
				g_current_char++;
			}
		}
		if (*p)
			p++; // advance to next character
		//Serial.println(p);
	}

	// refresh immediately ... we'll see how this works out for us
	refresh_text_display();
}

/* ---
#### oledPrint()
append a `printf()` style line to the OLED text buffer and display it immediately, scrolling it into view as needed

  - input: id **char ptr** printf style format for the message to output; followed by the arg_list
--- */
void oledPrintf(const char *fmt...) {
	static char oled_line_buffer[MAX_TEXT + 1];
	va_list args;
	va_start(args, fmt);

	oled_line_buffer[0] = 0;
	vsnprintf(oled_line_buffer, MAX_TEXT, fmt, args);

	//oled_line_buffer[MAX_CHARS - 2] = '\n';
	oled_line_buffer[MAX_TEXT - 1] = 0;
	oledPrint(oled_line_buffer, true);

	va_end(args);
}

#define LONG_PRESS_DURATION 750 // milliseconds

/* ---
#### oledButtonState()
Return the current button state

The range of button states allows the caller to track both the start of a press event or long press as well as the eventual release.

 The button states are:
  - IDLE - the button has no interaction
  - PRESS - the button has been pressed
  - LONG PRESS - the button has been pressed and held down for more than 750 ms
  - RELEASE - the button has been released after being pressed
  - LONG RELEASE - the button has been released after being held for more than 750 ms
  - UNKNOWN - something went wrong ... probably because the programmer of this code didn't have enough espresso and was asleep at the wheel

**Note**: This is a _polling function, not interrupt driven_

 - return: **uint8_t** one of the button state defined values
--- */
uint8_t oledButtonState() {
	static unsigned long time_pressed = 0;
	static uint8_t current_state = BUTTON_IDLE;

	// button input is PULLUP, a low reading means it is pressed, a high reading means it is not
	if (!digitalRead(PIN_BUTTON)) {
		switch (current_state) {
			case BUTTON_IDLE: {
				current_state = BUTTON_PRESS;
				time_pressed = millis();
				//Serial.println("PRESS");
			} break;
			case BUTTON_PRESS: {
				if ((time_pressed + LONG_PRESS_DURATION) < millis()) {
					current_state = BUTTON_LONG_PRESS;
					//Serial.println("LONG PRESS");
				}
			} break;
			default: {
				//current_state = BUTTON_UNKNOWN;
			} break;
		}
	} else {
		switch (current_state) {
			case BUTTON_PRESS: {
				current_state = BUTTON_RELEASE;
				time_pressed = 0;
				//Serial.println("RELEASE");
			} break;
			case BUTTON_LONG_PRESS: {
				current_state = BUTTON_LONG_RELEASE;
				time_pressed = 0;
				//Serial.println("LONG RELEASE");
			} break;
			case BUTTON_RELEASE:
			case BUTTON_LONG_RELEASE: {
				current_state = BUTTON_IDLE;
				//Serial.println("RESET");
			} break;
			default: {
				//current_state = BUTTON_UNKNOWN;
			} break;
		}
	}
	return current_state;
}

/* ---
#### oledWaitForButton()
Wait for a button press/release before continuing

The button state is ignored until it is IDLE then waits for the button release event. The function only returns once the button RELEASE has been detected.

**Note**: This is a _polling function, not interrupt driven_ and this is a _blocking operation_.
--- */

void oledWaitForButton(bool prompt) {
	// we wait for a button press
	// flush any button events
	if (prompt)
		MESSAGE(" --- PUSH THE BUTTON --- "); // ode to Jack Carter

	while (oledButtonState() != BUTTON_IDLE)
		;
	// wait for a button release
	while (oledButtonState() != BUTTON_RELEASE)
		;

	// UX experiment: if we just completed a FLASH, stay on the menu - this facilitates repeating the operation
	if (g_flashing) {
		g_flashing = false;
		g_oled_mode = OLED_MODE_MENU;
		}
	else
		g_oled_mode = OLED_MODE_FILES;

	g_display_dirty = true;
}
