#ifndef __OLED_H
#define __OLED_H

bool oledInit();
void oledLoop();

void oledPrint(const char *msg, bool wrap);
void oledPrintf(const char *fmt...);


#define BUTTON_IDLE 		0
#define BUTTON_PRESS		1
#define BUTTON_LONG_PRESS	2
#define BUTTON_RELEASE		3
#define BUTTON_LONG_RELEASE	4
#define BUTTON_UNKNOWN		99

void oledRefresh();
void oledClear(bool home);
uint8_t oledButtonState();
void oledWaitForButton(bool prompt);

#define OLED_ICON_STATE_UNK 	-1
#define OLED_ICON_STATE_OFF 	 0
#define OLED_ICON_STATE_ON		 1

void oledUpdateIcons(int8_t rx, int8_t tx, int8_t con);

#endif
