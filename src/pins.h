#ifndef __PINS_H
#define __PINS_H

/* ***************************************************************************
* File:    pins.h
* Date:    2019.09.09
* Author:  Bradan Lane Studio
* ******************************************************************************/

/* ---
--------------------------------------------------------------------------
### ESP32 GPIO pin assignments

The WS2812 RGB LEDs require one output pin.
The HSPI requires four pins, three of which are fixed and one may be changed

*The ESP32 has more pins than an ESP8266. I'm not going ot bother to outline them all here.*
--- */

// For portability, we reference pins by their GPIO# and not the board specific identifiers.


/* ---
The second USART is used to interface to the AVR.
--- */
#define PIN_USART_TX    17
#define PIN_USART_RX    16

/* ---
The **LilyGO TTGO ESP32 w/ 18650 Charging & 0.96" OLED** connects the BOOT button to GPIO0.
--- */
#define PIN_BUTTON		0

/* ---
The OLED uses I2C on the default pins.
--- */
#define PIN_I2C_SCL		4
#define PIN_I2C_SDA		5

/* ---
The ESP32's second UART is used to interface to the AVR.
--- */
#define PIN_ISP_CLK 14
#define PIN_ISP_MISO    12
#define PIN_ISP_MOSI    13
#define PIN_ISP_RESET   27



// ----------------------------------------------------------------------
// -- a few generally useful macros -------------------------------------
// ----------------------------------------------------------------------

// TODO these are not pin related so there should be a common include

#define VERBOSE(fmt, ...) Serial.printf(fmt, ##__VA_ARGS__)
//#define VERBOSE(fmt, ...) ((void)0)

/* ---
#### MESSAGE(...)
Macro to sending `printf()` style content to both the OLED and Serial console.
--- */
//#define MESSAGE(fmt, ...) Serial.printf(fmt, ##__VA_ARGS__)
#define MESSAGE(fmt, ...) oledPrintf(fmt, ##__VA_ARGS__)
//#define MESSAGE(fmt, ...) ((void)0)

#endif
