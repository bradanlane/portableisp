/* ***************************************************************************
* File:    web.cpp
* Date:    2018.02.16
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined
* under the MIT License
*
* ******************************************************************************/

/* ---
--------------------------------------------------------------------------
### Web API
Provides the web server and websockets interfaces.

The web files are stored in SPIFFS. The files may be uploaded either as an spiffs.bin or
individual files may be uploaded using the web interface: `http://webisp.local/upload.html`

The UI is rendered as HTML with JavaScript and sends JSON messages through the web socket interface.

The web sockets interface may be extended to add additional events and interactions.
Add new commands to `web_socket_event()` within the section for `WStype_TEXT`.
--- */

#include "filesys.h"
#include <WebServer.h>
#include <WebSocketsServer.h>

// local includes
#include "oled.h"
#include "pins.h"
#include "wifi.h"

#define MAX_TELNET_TEXT 160

//#define NEED_SEBSOCKET					// uncomment this if there will be a websocket interface

/* the is based from example code at: https://github.com/tttapa/ESP8266/Examples */

static WiFiServer g_telnet_server(TELNET_PORT); // TODO need a more secure option than telnet
static WiFiClient g_telnet_client;
static WebServer g_web_server(80); // create a web server on port 80
#ifdef NEED_WEBSOCKET
static WebSocketsServer g_web_socket(81); // create a websocket server on port 81
static StaticJsonDocument<512> g_json_data;
#endif

/*__________________________________________________________HELPER_FUNCTIONS__________________________________________________________*/

static bool strend(const char *s1, const char *s2) {
	const char *p = &(s1[(strlen(s1) - strlen(s2))]);
	//Serial.printf("testing tail of %s as %s with %s\n", s1, p, s2);
	if (!strcmp(p, s2))
		return true;
	return false;
}

static const char *get_content_type(const char *filename) { // determine the filetype of a given filename, based on the extension
	if (strend(filename, ".html"))
		return "text/html";
	else if (strend(filename, ".css"))
		return "text/css";
	else if (strend(filename, ".js"))
		return "application/javascript";
	else if (strend(filename, ".ico"))
		return "image/x-icon";
	else if (strend(filename, ".gz"))
		return "application/x-gzip";
	return "text/plain";
}

/*__________________________________________________________SERVER_HANDLERS__________________________________________________________*/
#ifdef NEED_WEBSOCKET
static void web_socket_event(uint8_t num, WStype_t type, uint8_t *payload, size_t length) { // When a WebSocket message is received
	switch (type) {
		case WStype_DISCONNECTED: { // if the websocket is disconnected
			Serial.printf("[%u] Disconnected!\n", num);
		} break;

		case WStype_CONNECTED: { // if a new websocket connection is established
			IPAddress ip = g_web_socket.remoteIP(num);
			Serial.printf("[%u] Connected from %d.%d.%d.%d url: %s\n", num, ip[0], ip[1], ip[2], ip[3], payload);
		} break;

		case WStype_TEXT: { // if new text data is received
			Serial.printf("websocket payload: [%u] %s\n", num, payload);
			DeserializationError err = deserializeJson(g_json_data, payload);
			if (err) {
				// TODO need to return a message that we failed
				Serial.println("failed to parse the json payload");
				return;
			}
			if (g_json_data["cmd"] == "foo") {
				float left = g_json_data["bar1"];
				float right = g_json_data["bar2"];
			}
		} break;

		default: {
			Serial.println("unsupported socket event type");
		} break;
	}
}
#endif

static void handle_file_upload() { // upload a new file to the SPIFFS
	HTTPUpload &upload = g_web_server.upload();
	if (upload.status == UPLOAD_FILE_START) {
		filesysSaveStart(upload.filename.c_str());
	} else if (upload.status == UPLOAD_FILE_WRITE) {
		filesysSaveWrite(upload.buf, upload.currentSize);
	} else if (upload.status == UPLOAD_FILE_END) {
		if (filesysSaveFinish()) {
			g_web_server.send(303, "text/plain", "303: success");
			oledClear(true);
		} else
			g_web_server.send(500, "text/plain", "500: couldn't create file");
	}
}

// we embed the upload page so it is always available, even if the spiffs.bin has not been loaded
const char g_upload_page[] PROGMEM = R"=====(
<!DOCTYPE html><html><head><title>Default File Upload</title></head>
<body><h1>PortableISP File Upload</h1>
<p>Select a new file to upload to the PortableISP.</p>
<form method="POST" enctype="multipart/form-data"><input type="file" name="data"><input class="button" type="submit" value="Upload"></form>
</body></html>
)=====";

static bool handle_file_read(String path) { // send the right file to the client (if it exists)
	Serial.println("handle_file_read: " + path);
	if (path.endsWith("/"))
		path += "index.html"; // If a folder is requested, send the index file

	const char *content_type = get_content_type(path.c_str()); // Get the MIME type

	bool found = false;

#if 0
	String pathWithGz = path + ".gz";	// If the file exists, either as a compressed archive, or normal
	if (filesysExists(pathWithGz)) { // If there's a compressed version available
		path += ".gz";				 // Use the compressed verion
		found = true;
	} else
#endif
	if (filesysExists(path.c_str()))
		found = true;

	if (found) {
		File file = filesysOpen(path.c_str());
		g_web_server.streamFile(file, content_type); // Send it to the client
		filesysClose(file);
		// Serial.printf("sent %d bytes from %s as type %s\n", sent, path.c_str(), contentType.c_str());
		return true;
	}

	// fallback for upload if the file does not yet exist
	if (path.endsWith("upload.html")) {
		g_web_server.send(200, content_type, g_upload_page); // Send the backup upload page
	}
	Serial.printf("%s not found\n", path.c_str()); // If the file doesn't exist, return false
	return false;
}

static void handle_file_request() {				 // if the requested file or page doesn't exist, return a 404 not found error
	if (!handle_file_read(g_web_server.uri())) { // check if the file exists in the flash memory (SPIFFS), if so, send it
		g_web_server.send(404, "text/plain", "404: File Not Found");
	}
}

static void handle_telnet_requests() {
	static uint8_t buf[MAX_TELNET_TEXT + 1 + 1]; // buffer + '\n' + 0
	memset(buf, 0, (MAX_TELNET_TEXT + 1 + 1));
	// the telnet connection uses Serial2 UART

	// handle any connection requests
	// we only support one telnet connect at a time; a second one is ejected
	if (g_telnet_server.hasClient()) {
		//Serial.println("telnet client requester");
		if (g_telnet_client)
			g_telnet_client.stop();
		g_telnet_client = g_telnet_server.available();
	} else {
		// we lost the connection
		if (g_telnet_client && !g_telnet_client.connected())
			g_telnet_client.stop();
	}

	if (g_telnet_client && g_telnet_client.connected()) {
		oledUpdateIcons(OLED_ICON_STATE_UNK, OLED_ICON_STATE_UNK, OLED_ICON_STATE_ON);

		// Get data from the telnet client and push it to the UART
		if (g_telnet_client.available())
			oledUpdateIcons(OLED_ICON_STATE_ON, OLED_ICON_STATE_UNK, OLED_ICON_STATE_UNK);

		while (g_telnet_client.available()) {
			Serial2.write(g_telnet_client.read());
		}
		oledUpdateIcons(OLED_ICON_STATE_OFF, OLED_ICON_STATE_UNK, OLED_ICON_STATE_UNK);
	} else
		oledUpdateIcons(OLED_ICON_STATE_UNK, OLED_ICON_STATE_UNK, OLED_ICON_STATE_OFF);

	// get any data from UART and push it to telnet client
	if (Serial2.available()) {
		size_t len = Serial2.available();
		oledUpdateIcons(OLED_ICON_STATE_UNK, OLED_ICON_STATE_ON, OLED_ICON_STATE_UNK);

		if (len > MAX_TELNET_TEXT)
			len = MAX_TELNET_TEXT;

		Serial2.readBytes(buf, len);

		// Push UART data to telnet client
		if (g_telnet_client && g_telnet_client.connected()) {
			g_telnet_client.write(buf, len);
			//g_telnet_client.flush();
		}

		// if the buffer is not null terminated, do so but be sure there is space
		if (buf[len-1] != 0) {
			if (len == MAX_TELNET_TEXT)
				len--;
			buf[len] = 0;
		}
		Serial.printf("\nbuf[%d]:\n", len);
		oledPrint((char *)buf, true);

		oledUpdateIcons(OLED_ICON_STATE_UNK, OLED_ICON_STATE_OFF, OLED_ICON_STATE_UNK);
	}
}

/*__________________________________________________________SETUP_FUNCTIONS__________________________________________________________*/

#ifdef NEED_WEBSOCKET
static bool start_socket_server() {
	g_web_socket.begin();					// start the websocket server
	g_web_socket.onEvent(web_socket_event); // if there's an incomming websocket message, go to function 'web_socket_event'
	Serial.println("WebSocket server started");
	return true; // no way of knowing if we failed anything
}
#endif

static bool start_web_server() {
	// Start a HTTP server with a file read handler and an upload handler
	// If a POST request is sent to the /edit.html address,
	g_web_server.on("/upload.html", HTTP_POST, []() { g_web_server.send(200, "text/plain", ""); }, handle_file_upload); // go to 'handle_file_upload'

	// if someone requests any other file or page, go to function 'handleNotFound' and check if the file exists
	g_web_server.onNotFound(handle_file_request);

	g_web_server.begin(); // start the HTTP server
	Serial.println("HTTP server started.");
	return true; // no way ot knowing if we failed anything
}

static bool start_telnet_server() {
	// Start Telnet server
	g_telnet_server.begin();
	g_telnet_server.setNoDelay(true);
	return true;
}

#if 0
/* ---
#### webTelnetPrint()
if a telnet client is active, send the message and a newline
--- */
void webTelnetPrint(const char *msg) {
	if (g_telnet_client && g_telnet_client.connected()) {
		g_telnet_client.write(msg, strlen(msg));
		g_telnet_client.write("\n", 1);
		oledPrint(msg, false);
	}
}
#endif

/* ---
#### webInit()
Performs all necessary initialization of both the webserver on port 80 and the websockets interface on port 81.
Must be called once before using any of the other web functions.
 - return: **bool** `true` on success and `false` on failure
--- */
bool webInit() {
	bool success = true;
	// Start a WebSocket server and then HTTP server with handlers
#ifdef NEED_WEBSOCKET
	success &= start_socket_server();
#endif
	success &= start_web_server();
	success &= start_telnet_server();
	return (success);
}

/* ---
#### webLoop()
Give the web interfaces an opportunity to respond to page requests and websockets messages
--- */
void webLoop() {
#ifdef NEED_WEBSOCKET
	g_web_socket.loop(); // constantly check for websocket events
#endif
	g_web_server.handleClient(); // run the server
	handle_telnet_requests();
}
