#ifndef _WIFI_H
#define _WIFI_H

#include <Arduino.h>

#define OTA_PORT 8266 	// its the default :-)
#define TCP_PORT 8265 	// its close
#define TELNET_PORT 23	// its default

bool wifiInit();
void wifiLoop();

bool otaInit();
void otaLoop();

bool wifiIsConnected();
bool wifiConnectionLost();
bool wifiIsHotspot();

char *wifiAddress();
uint8_t wifiAddressEnding();

void wifiOff();
void wifiOn();

#endif
